import re
import sys
import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS 
from imageio import imread 
from arabic_reshaper import arabic_reshaper
from bidi.algorithm import get_display
from hazm import Normalizer
from emojiremover import remove_emojis

# This is the base image for your cloud
cloud_mask = imread('./twitter_mask.png')

# This removes the stop words, thanks to kharazi and my dear pointer for sharing their stop-word lists
stop_words = open("stopwords_fa")
stop_words = [word.rstrip('\n') for word in stop_words.readlines()]

stop_words_additional = open("stopwords_fa_additional.txt")
stop_words_additional = [word.rstrip('\n') for word in stop_words_additional.readlines()]

stop_words = stop_words + stop_words_additional

text_file = open(sys.argv[1])
text_file = text_file.readlines()
text = " ".join(text_file)

'''
This part is responsible for cleaning the text! 
1. It removes all links 
2. Removes mentions 
3. Removes Latin phrases 
4. Cleans stop words 
''' 
text = text.split(' ')
no_url = [] 
url_pattern = r'.*\s+https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w\/_\.]*(\?\S+)?)?)?.*'

for line in text:
    if not re.match(url_pattern, line):
        no_url.append(line)

no_mention = [] 
mention_pattern = r'.*\s+(@).*'

for line in no_url: 
    if not re.match(mention_pattern, line):
        no_mention.append(line)

no_latin = [] 
latin_pattern = r'.*[a-zA-Z].*'

for word in no_mention:
    if not re.match(latin_pattern, word):
        no_latin.append(word)

normal_text = [] 
n = Normalizer()
for word in no_latin:
    normal_text.append(n.normalize(word))


clean_text = [] 
for word in normal_text:
    if word not in stop_words:
        word = word.replace('\u2066', '')
        word = word.replace('\u2069', '')
        word = word.replace('\u2067', '')
        word = word.replace('\u2068', '')
        clean_text.append(word)


''' 
Here , we just make up our final text and clean the emojis, then we optimize our code for the Word Cloud library.
It also removes emojis 
''' 
text = " ".join(clean_text)
text = remove_emojis(text)
text = " ".join(text)
text = get_display(arabic_reshaper.reshape(text))

wc = WordCloud(font_path=sys.argv[2], background_color='white', width=1800, height=1800, stopwords=stop_words, mask=cloud_mask).generate(text) 
 
plt.imshow(wc)
plt.axis('off') 
plt.savefig('./wc.png', dpi=300) 
plt.close()
plt.show()
